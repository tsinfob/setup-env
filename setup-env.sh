#!/bin/bash

########################################################################
# Copyright (C) 2019 Radwin Ltd.
########################################################################
#
# Radwin Build Environment Setup Script
#
# Normally this is called as '. setup-env <builddir>'
#
# Setting custom HW platform 
#	MACHINE=<full-machine-name> . setup-env
# example:
#	MACHINE=rw-ls1043ardb . setup-env
#
# Setting development mode
#	DEVELOP=yes . setup-env
#
# WARNING:
#	Custom machine should be compatible with base rw-ls1046ardb machine
#

if [ -n "$BASH_SOURCE" ]; then
    SCRIPTNAME="$BASH_SOURCE"
elif [ -n "$ZSH_NAME" ]; then
    SCRIPTNAME="$0"
else
    SCRIPTNAME="$PWD/setup-env"
fi

if [ -z "$ZSH_NAME" ] && [ "$0" = "$BASH_SOURCE" ]; then
    echo "Error: This script needs to be sourced. Please run as '. $SCRIPTNAME'"
    exit 1
fi

TEMPLATECONF=../meta-radwin/conf
. $(dirname $(readlink -f "$SCRIPTNAME"))/../../poky/oe-init-build-env

# Create links to binary caches if they exist
if [ -d ../bincache/downloads -a ! -e downloads ]; then
  ln -s ../bincache/downloads downloads
fi
if [ -d ../bincache/sstate-cache -a ! -e sstate-cache ]; then
  ln -s ../bincache/sstate-cache sstate-cache
fi

# Set user-defined machine type
if [ -n "$MACHINE" ]; then
	sed -i "s/.*MACHINE.*/MACHINE ?= \"$MACHINE\"/" conf/local.conf
fi
# Enable development mode features
if [ -n "$DEVELOP" ]; then
	sed -i "s/#EXTRA_IMAGE_FEATURES/EXTRA_IMAGE_FEATURES/" conf/local.conf
fi